<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Ajaxordernotes extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'ajaxordernotes';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Ciren';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Order notes on the list');
        $this->description = $this->l('Create order notes from the order list');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->addTab() &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('actionAdminControllerSetMedia') &&
            $this->registerHook('actionAdminOrdersListingFieldsModifier');
    }

    private function addTab()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminAjaxOrderNotes';
        $tab->id_parent = Tab::getIdFromClassName('AdminAdmin');
        $tab->module = $this->name;
        $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->l('Order notes');
        $tab->add();

        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitAjaxordernotesModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    public function hookBackOfficeHeader()
    {
        if (!Module::isEnabled($this->name) && Tools::getValue('controller') == "AdminOrders") {
            return false;
        }

        return '<script>
        var admin_ajaxordernotes_ajax_url = ' . (string) json_encode(
            pSQL($this->context->link->getAdminLink('AdminAjaxOrderNotes'))
        ) . ';var current_id_tab = ' . (int) $this->context->controller->id . ';
        </script>';
    }

    public function hookActionAdminOrdersListingFieldsModifier($params)
    {
        $params['join'] .= " LEFT JOIN `" . _DB_PREFIX_ . "ajaxordernotes` `aon` ON `a`.`id_order` = `aon`.`id_order`";
        $params['select'] .= ", `aon`.`note` AS 'aonnote'";

        $params['fields']['aonnote'] = array(
            'title' => $this->l('Note'),
            'class' => 'aon-input'
        );
    }

    public function hookActionAdminControllerSetMedia()
    {
        if (Tools::getValue('controller') == "AdminOrders" && method_exists($this->context->controller, 'addJquery')) {
            $this->context->controller->addJs($this->_path . 'views/js/back.js');
        }
    }
}
