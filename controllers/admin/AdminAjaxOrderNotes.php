<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminAjaxOrderNotesController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'ajaxordernotes';
        $this->identifier = 'id_ajaxordernotes';
        $this->tabClassName = 'AdminAjaxOrderNotes';
        $this->lang = false;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array(
          'delete' => array('text' => 'Delete selected', 'confirm' => 'Delete selected items?'),
        );
        $this->context = Context::getContext();

        parent::__construct();

        // $this->fields_list = array(
        //     'id_ajaxordernotes' => array(
        //         'title' => $this->l('Position'),
        //         'align' => 'text-center',
        //         'position' => 'position',
        //         'filter_key' => 'position',
        //         'class' => 'fixed-width-xs'
        //     ),
        //     'attribute' => array(
        //         'title' => $this->l('Attributes'),
        //         'align' => 'text-center',
        //         'filter_key' => 'attribute'
        //     ),
        //     'table' => array(
        //         'title' => $this->l('Table'),
        //         'align' => 'text-center',
        //         'filter_key' => 'table'
        //     ),
        //     'title' => array(
        //         'title' => $this->l('Title'),
        //         'align' => 'text-center',
        //         'class' => 'ec-titles'
        //     ),
        //     'type' => array(
        //         'title' => $this->l('Type'),
        //         'align' => 'text-center',
        //         'class' => 'ec-types'
        //     ),
        // );
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();

        if (empty($this->display)) {
            $this->page_header_toolbar_btn['desc-module-back'] = array(
                'href' => 'index.php?controller=AdminModules&configure=extracolumns&token=' .
                Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Other tabs'),
                'icon' => 'process-icon-back',
            );
            $this->page_header_toolbar_btn['desc-module-new'] = array(
                'href' => 'index.php?controller=' . $this->tabClassName . '&add' . $this->table . '&token=' .
                Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('Add a new column'),
                'icon' => 'process-icon-new',
            );
            $this->page_header_toolbar_btn['desc-module-reload'] = array(
                'href' => 'index.php?controller=' . $this->tabClassName . '&token=' .
                Tools::getAdminTokenLite($this->tabClassName),
                'desc' => $this->l('Reload'),
                'icon' => 'process-icon-refresh',
            );
        }
    }

    public function renderList()
    {
        if (!Configuration::get('id_extracolumns')) {
            $link = new Link();
            $link = $link->getAdminLink('AdminModules', true, [], ['configure' => 'extracolumns']);
            return Tools::redirectAdmin($link);
        }

        $id_extracolumns = Configuration::get('id_extracolumns');

        if (Tools::isSubmit('submitGlobalForm')) {
            $obj = new ExtraColumnsConfiguration();
            if ($obj->populateExtracolumns(
                $id_extracolumns,
                Tools::getValue('active'),
                Tools::getValue('replace')
            )) {
                $this->success[] = $this->l("Configuration saved");
            } else {
                $this->errors[] = $this->l("There was an error");
            }
        } elseif (Tools::isSubmit('EXTRACOLUMNS_QUICK_ADD')) {
            $obj = new ExtraColumnsConfiguration();
            if ($obj->add()) {
                $this->success[] = $this->l("Successfully added");
            } else {
                $this->errors[] = $this->l("There was an error");
            }
        }

        $this->addRowAction('delete');

        if (isset($this->success)) {
            $this->context->smarty->assign(array(
                'success' => $this->success
            ));
        }
        if (isset($this->warning)) {
            $this->context->smarty->assign(array(
                'warning' => $this->warning
            ));
        }
        if (isset($this->errors) && array_key_exists('0', $this->errors)) {
            $this->context->smarty->assign(array(
                'errors' => $this->errors
            ));
        }

        $config = Db::getInstance()->getRow(
            "SELECT `tab`, `active`, `replace`
            FROM `" . _DB_PREFIX_ . "extracolumns`
            WHERE `id_extracolumns` = '" . (int)$id_extracolumns . "'"
        );

        $this->context->smarty->assign(array(
            'active' => isset($config['active']) ? $config['active'] : 0,
            'replace' => isset($config['replace']) ? $config['replace'] : 0,
            'types' => $this->types,
            'quick_add' => isset($config['tab']) ? $this->quick_add[$config['tab']] : array(array()),
            'description' => $this->getDefaultList(),
            'id_extracolumns' => (int)$id_extracolumns
        ));

        $alert = $this->context->smarty->fetch($this->module->getLocalPath() . 'views/templates/admin/alert.tpl');
        $global_form = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/global_form.tpl');
        $renderList = $this->context->smarty->fetch($this->module->getLocalPath() .
        'views/templates/admin/renderList.tpl');

        return $alert . $global_form . parent::renderList() . $renderList;
    }

    public function ajaxProcessSendNote()
    {
        // AJAX DATAS :
        // AON_input
        // id_order

        $actual = (int) Db::getInstance()->getValue(
            "SELECT `id_ajaxordernotes`
            FROM `" . _DB_PREFIX_ . $this->table . "`
            WHERE `id_order` = '" . (int)Tools::getValue('id_order') . "'
            AND `id_employee` = '" . (int)$this->context->employee->id . "'"
        );

        if (!$actual) {
            $return = Db::getInstance()->execute(
                "INSERT INTO `" . _DB_PREFIX_ . $this->table . "` (`id_order`, `id_employee`, `note`)
                VALUES ('" . (int)Tools::getValue('id_order') . "',
                '" . (int)$this->context->employee->id . "',
                '" . pSQL(Tools::getValue('AON_input')) . "')"
            );
        } else {
            $return = Db::getInstance()->execute(
                "UPDATE `" . _DB_PREFIX_ . $this->table . "`
                SET `note` = '" . pSQL(Tools::getValue('AON_input')) . "', `date_upd` = NOW()
                WHERE `id_ajaxordernotes` = '" . (int)$actual . "'"
            );
        }

        if ($return) {
            die(Tools::jsonEncode(array('success' => 1, 'message' => $this->l('Successfull update.'))));
        } else {
            die(Tools::jsonEncode(array('success' => 0, 'message' => $this->l('An error has occured.'))));
        }
    }
}
