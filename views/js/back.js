/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$('document').ready(function() {
    let value;
    $('td.aon-input').each(function() {
        value = $.trim($(this).text()) == "--" ? "" : $.trim($(this).text()).replace(/"/g, '&quot;');
        $(this).html("<input type='text' class='aon-input-text' value=\"" + value + "\">");
    });

    $('td.aon-input').removeAttr('onclick');
    $('td.aon-input').removeClass('pointer');

    $('.aon-input-text').on('change', function(){
        let id_order = Number($(this).parent('td').siblings(':eq(1)').text());
        $.ajax({
            type: 'POST',
            url: admin_ajaxordernotes_ajax_url,
            dataType: 'json',
            data: {
                controller : 'AdminAjaxOrderNotes',
                action : 'sendNote',
                ajax : true,
                id_order: id_order,
                AON_input : $(this).val(),
            },
            complete: function(data) {
                if (data.readyState == 4) {
                    if (data.responseJSON.success) {
                        $.growl.notice({ title: "", message: data.responseJSON.message });
                    } else {
                        $.growl.error({ title: "", message: data.responseJSON.message });
                    }
                } else {
                    $.growl.error({ title: "", message: "An error has occured." });
                }
            }
        });
    });
})
